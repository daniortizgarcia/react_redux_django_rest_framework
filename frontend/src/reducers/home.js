import { 
  HOME_PAGE_LOADED, 
  HOME_PAGE_UNLOADED, 
  HOME_CHANGE_INPUT, 
  HOME_SLIZER_SUBMIT 
} from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case HOME_PAGE_LOADED:
    console.log('HOME_PAGE_LOADED',action.payload)
      return {
        ...state,
        hashtags: action.payload[1].results
      };
    case HOME_CHANGE_INPUT:
      return {
        ...state,
        slizer : action.slizer
      }
    case HOME_SLIZER_SUBMIT:
      return {
        ...state,
        slizer : ''
      }
      
    case HOME_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
