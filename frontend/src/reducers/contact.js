import {
	CONTACT,
	UPDATE_FIELD_CONTACT,
	VALIDATE_FORM_CONTACT,
	CLEAR_CONTACT
} from '../constants/actionTypes';

export default (state = {}, action) => {
	switch (action.type) {
		case CONTACT:
			return {
				...state,
				errors: 	 action.payload ? action.payload.errors : null,
				state: 		 action.payload ? action.payload.status : null,
				messageSucc: action.payload ? action.payload.message : null,
			};
		case VALIDATE_FORM_CONTACT:
			if (!action.value.name)
				return { ...state, errorMes: "The name is empty", errorState: false, errors: null };
			else if (action.value.name.length < 3 || action.value.name.length > 15)
				return { ...state, errorMes: "The name has to be between 3 to 15 letters.", errorState: false, errors: null };
			else if (!action.value.email)
				return { ...state, errorMes: "The email is empty", errorState: false, errors: null};
			else if (!action.value.email.match(/^(([^<>()\\.,;:\s@"]+(\.[^<>()\\.,;:\s@"]+)*)|(".+"))@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
				return { ...state, errorMes: "The email is invalid", errorState: false, errors: null};
			else if (action.value.subject === "Select one option" || !action.value.subject)
				return { ...state, errorMes: "Select one subject", errorState: false, errors: null };
			else if (!action.value.message)
				return { ...state, errorMes: "The message is empty", errorState: false, errors: null };
			else if (action.value.message.length < 15 || action.value.message.length > 100)
				return { ...state, errorMes: "The message has to be between 15 to 100 letters.", errorState: false, errors: null };
			else 
				return { ...state, errorMes:"", errorState: true, errors: null };
		case UPDATE_FIELD_CONTACT:
			return { ...state, [action.key]: action.value };
		case CLEAR_CONTACT:
			return {};
		default:
			return state;
	}
};
