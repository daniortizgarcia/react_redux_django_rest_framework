import {
	POST_PAGE_LOADED,
	CLOSE_MODAL,
	ADD_POST_OPINION,
	POST_CHANGE_INPUT,
  } from '../constants/actionTypes';
  
export default (state = {}, action) => {
	switch (action.type) {
		  case POST_PAGE_LOADED:
        	return {
          		...state,
          		showModal: true,
          		postDetail: action.payload[0],
          		postOpinions: action.payload[1].results,
			};
		case POST_CHANGE_INPUT:
			return { 
				...state, 
				opinion: action.opinion 
			};
		case ADD_POST_OPINION:
			return {
				...state,
				opinion: '' 
			}
		case CLOSE_MODAL:
			return {
				...state,
				showModal: false,
			};
      	default:
        	return {
				...state,
				showModal:false
			};
    }
};
  