import {
	PROFILE_PAGE_LOADED,
	PROFILE_PAGE_UNLOADED,
	FOLLOW_USER,
	UNFOLLOW_USER,
	GET_PROFILES,
	HEADER_CHANGE_INPUT,
	HEADER_LOAD_AUTO,
	HEADER_LIST_PROFILE,
	CHANGE_VIEW_PROFILE,
	PROFILE_CHANGE_INPUT,
} from '../constants/actionTypes';

export default (state = {}, action) => {
	switch (action.type) {
		case GET_PROFILES:
			let prof = action.payload.results;
			prof = JSON.parse(JSON.stringify(prof).split('"username":').join('"title":'));
			prof = JSON.parse(JSON.stringify(prof).split('"bio":').join('"description":'));
			return {
				...state,
				profilesAutoG: prof,
				isLoadingAuto: false
			};
		case HEADER_CHANGE_INPUT:
			return {
				...state,
				valueInput: action.username,
				isLoadingAuto: true
			};
		case HEADER_LOAD_AUTO:
			action.profiles.map(profile => delete profile.following)
			return {
				...state,
				profilesAuto: action.profiles,
				isLoadingAuto: false
			};
		case PROFILE_PAGE_LOADED:
			return {
				...state,
				...action.payload[0].profile,
				view: 'posts',
				followingUser: action.payload[3].filter(prof => { return prof.following === true }),
				followersUser: action.payload[2],
			};
		case CHANGE_VIEW_PROFILE:
			return {
				...state,
				view: action.view
			}
		case PROFILE_PAGE_UNLOADED:
			return state;
		case HEADER_LIST_PROFILE:
			return {
				...state,
				valueInput: action.username,
			};
		case PROFILE_CHANGE_INPUT:
			return {
				...state,
				slizer : action.slizer
			}
		case FOLLOW_USER:
		case UNFOLLOW_USER:
			return {
				...state,
				...action.payload.profile
			};
		default:
			return state;
	}
};
