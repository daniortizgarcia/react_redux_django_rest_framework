import {
    HOME_PAGE_LOADED,
    HOME_PAGE_UNLOADED,
    SET_PAGE,
    APPLY_HASHTAG_FILTER,
    PROFILE_PAGE_LOADED,
    HOME_CHANGE_FEED
  } from '../constants/actionTypes';
  
export default (state = {}, action) => {
    switch (action.type) {
        case HOME_CHANGE_FEED:
            return {
                ...state,
                posts: action.payload.results.filter(post => post.author.username !== action.username),
                pager: action.pager,
                postsCount: action.payload.count,
                currentPage: 0
            };
        case HOME_PAGE_LOADED:
            return {
                ...state,
                posts: action.payload[0].results,
                pager: action.pager,
                postsCount: action.payload[0].count,
                currentPage: 0
            };
        case HOME_PAGE_UNLOADED:
            return {};
        case APPLY_HASHTAG_FILTER:
            return {
                ...state,
                posts: action.payload.results,
                pager: action.pager,
                postsCount: action.payload.count,
                currentPage: 0,
                hashtag: action.hashtag
            }
        case PROFILE_PAGE_LOADED:
            return {
                ...state,
                pager: action.pager,
                posts: action.payload[1].results,
                postsCount: action.payload[1].count,
                currentPage: 0
            }
        case SET_PAGE:
            return {
                ...state,
                posts: action.payload.results.filter(post => post.author.username !== action.username),
                postsCount: action.payload.count,
                currentPage: action.page
            }
        default:
            return state;
    }
};
  