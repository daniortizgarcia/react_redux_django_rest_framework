import {
	CHAT_PAGE_LOADED,
	CHAT_SHOW_MESSAGES,
	CHAT_CHANGE_INPUT,
	ADD_MESSAGE,
	CREATE_CHAT,
} from '../constants/actionTypes';

export default (state = {}, action) => {
	let filterProfile = (chat, profile) => {
		let followingUser = [];
		let stateProf = false;
		profile.filter(prof => { return prof.following === true }).map( prof => {
			stateProf = true;
			if(chat) {
				chat.map( chat => {
					if(prof.username === chat.creator.username || prof.username === chat.chater.username)
						stateProf = false;
					return true;
				});
			}
			if(stateProf)
				followingUser.push(prof)
			return true;
		});
		return followingUser;
	}
	switch (action.type) {
		case CHAT_PAGE_LOADED:
			let followingUserFilter = filterProfile(action.payload[0], action.payload[1])
			return {
				...state,
				chats: action.payload[0],
				followingUser: followingUserFilter,
			};
		case CHAT_SHOW_MESSAGES:
			return {
				...state,
				showChat: state.chats.filter( chat => chat[action.chater].username === action.username)[0]
			};
		case CHAT_CHANGE_INPUT:
			return {
				...state,
				inputChat: action.message
			};
		case ADD_MESSAGE: 
			return {
				...state,
				inputChat: '',
				showChat: action.payload,
			}
		case CREATE_CHAT:
			if(state.chats.length > 0) state.chats[state.chats.length + 1] = action.payload; else state.chats = [action.payload];
			followingUserFilter = filterProfile(state.chats, state.followingUser)
			return {
				...state,
				chats: state.chats,
				followingUser: followingUserFilter,
				showChat: action.payload,
			}
		default:
			return {
				...state
			};
	}
};
  