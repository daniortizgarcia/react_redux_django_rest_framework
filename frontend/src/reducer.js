import auth from './reducers/auth';
import { combineReducers } from 'redux';
import common from './reducers/common';
import home from './reducers/home';
import profile from './reducers/profile';
import contact from './reducers/contact';
import postList from './reducers/postList';
import post from './reducers/post';
import chat from './reducers/chat';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
  auth,
  common,
  home,
  profile,
  contact,
  postList,
  post,
  chat,
  router: routerReducer
});
