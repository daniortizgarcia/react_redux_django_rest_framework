import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
//import { request } from 'http';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'http://localhost:8000/api';

const encode = encodeURIComponent;
const responseBody = res => res.body;

let token = null;
const tokenPlugin = req => {
	if (token) {
		req.set('authorization', `Token ${token}`);
	}
}

const requests = {
	del: url =>
		superagent.del(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
	get: url =>
		superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
	put: (url, body) =>
		superagent.put(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
	post: (url, body) =>
		superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody)
};

const Auth = {
	current: () =>
		requests.get('/user'),
	login: (email, password) =>
		requests.post('/users/login', { user: { email, password } }),
	register: (username, email, password) =>
		requests.post('/users', { user: { username, email, password } }),
	save: user =>
		requests.put('/user', { user })
};

const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;


const Hashtags = {
	getAll: () => requests.get('/hashtags')
};

const Contact = {
	sendEmail: (data) => requests.post('/contact', { email: data })
}

const Posts = {
	getAll: page => requests.get(`/posts?${limit(10, page)}`),
	getFeed: page => requests.get(`/posts/feed/?${limit(10, page)}`),
	getOne: post => requests.get(`/posts/${post}`),
	byHashtag: (hashtag, page) => requests.get(`/posts?hashtags=${encode(hashtag)}&${limit(10, page)}`),
	create: post => requests.post('/posts', { post }),
	byAuthor: (author, page) =>
		requests.get(`/posts?author=${encode(author)}&${limit(5, page)}`),
};

const Opinions = {
	forPost: id => requests.get(`/posts/${id}/opinions`),
	create: (id, opinion) => requests.post(`/posts/${id}/opinions`, { opinion }),
}


const Profile = {
	getAll: () =>
		requests.get(`/profiles`),
	getFeed: (username, condition) =>
		requests.get(`/profiles/${username}/feed/${condition}`),
	follow: username =>
		requests.post(`/profiles/${username}/follow`),
	get: username =>
		requests.get(`/profiles/${username}`),
	unfollow: username =>
		requests.del(`/profiles/${username}/follow`)
};

const Chat = {
	getChats: username =>
		requests.get(`/chats?username=${encode(username)}`),
	createChat: (chat, chater) => 
		requests.post('/chats', { chat, chater }),
	addMessage: (id, chat) =>
		requests.put(`/chat/${id}`, { chat }),
};

export default {
	Auth,
	Profile,
	Contact,
	Posts,
	Hashtags,
	Opinions,
	Chat,
	setToken: _token => { token = _token; }
};
