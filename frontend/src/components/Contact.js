import React from 'react';
import { connect } from 'react-redux';
import agent from '../agent';
import toastr from 'toastr'

import {
    UPDATE_FIELD_CONTACT,
    CONTACT,
    VALIDATE_FORM_CONTACT,
    TOASTR_OPTIONS,
    CLEAR_CONTACT
} from '../constants/actionTypes';
toastr.options = TOASTR_OPTIONS

const mapStateToProps = state => ({ ...state.contact });

const mapDispatchToProps = dispatch => ({
    onChangeField: (value,key) =>
        dispatch({ type: UPDATE_FIELD_CONTACT, key: key, value }),
    validateForm: value =>
        dispatch({type: VALIDATE_FORM_CONTACT, value}),
    clearContact: () =>
        dispatch({type: CLEAR_CONTACT}),
    onSubmit: data =>
        dispatch({ type:CONTACT, payload: agent.Contact.sendEmail(data)})
})

class Contact extends React.Component {
    constructor(){
        super();
        this.changeName = ev => this.props.onChangeField(ev.target.value,"name");
        this.changeEmail = ev => this.props.onChangeField(ev.target.value,"email");
        this.changeMessage = ev => this.props.onChangeField(ev.target.value,"message");
        this.changeSubject = ev => this.props.onChangeField(ev.target.value,"subject");
        this.submitForm = (data) => ev => {
            ev.preventDefault();
            this.props.validateForm(data);
            setTimeout(()=>{
                if(this.props.errorState){
                    this.props.onSubmit(data);
                    setTimeout(()=>{
                        if(this.props.state){
                            toastr.success(this.props.messageSucc);
                            this.props.clearContact();
                            this.props.history.push('/');
                        }else{
                            if(this.props.errors.name)
                                this.props.errors.name.forEach(element => toastr.error(element));
                            if(this.props.errors.email)
                                this.props.errors.email.forEach(element => toastr.error(element));
                            if(this.props.errors.subject)
                                this.props.errors.subject.forEach(element => toastr.error(element));
                            if(this.props.errors.message)
                                this.props.errors.message.forEach(element => toastr.error(element));
                        }
                    },1500)
                }
            },500)
            
        };
    }

    render(){
        const name = this.props.name;
        const email = this.props.email;
        const subject = this.props.subject;
        const message = this.props.message;
        const styles = {
            height: "auto"
        }

        return(
            <article className="d-flex align-items-center flex-column pb-4">
                <h1 className="d-flex justify-content-center mt-3 mb-3">Contact</h1>
                <label className="text-danger">{this.props.errorMes}</label>
                <form className="w-75" onSubmit={this.submitForm({name,email,subject,message})}>
                    <fieldset className="form-group">
                        <input 
                            className="form-control form-control-lg"
                            type="text"
                            value={name || ''}
                            placeholder="Name"
                            onChange={this.changeName} ></input>
                    </fieldset>
                    
                    <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="email"
                            value={email || ''}
                            placeholder="Email"
                            onChange={this.changeEmail} ></input>
                    </fieldset>

                    <fieldset className="form-group">
                        <select 
                            className="form-control form-control-lg" 
                            style={styles}
                            value={subject || ''}
                            onChange={this.changeSubject}>
                                <option>Select one option</option> 
                                <option>I can't register</option>
                                <option>I can't sing in</option>
                                <option>It won't let me write a comment</option>
                                <option>I can't open a chat with someone</option>
                        </select>
                    </fieldset>

                    <fieldset className="form-group">
                        <textarea
                            className="form-control form-control-lg"
                            placeholder="Message"
                            value={message  || ''}
                            onChange={this.changeMessage} ></textarea>
                    </fieldset>

                    <fieldset>
                        <button 
                            className="btn btn-lg btn-primary pull-xs-right" 
                            type="submit">Send</button>
                    </fieldset>
                </form>
            </article>
        );
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Contact)
