import React from 'react';
import { Link } from 'react-router-dom';
import { Search, Grid } from 'semantic-ui-react'
import agent from "../agent";
import { connect } from 'react-redux';
import "semantic-ui-css/semantic.min.css";

import {
	GET_PROFILES,
	HEADER_CHANGE_INPUT,
	HEADER_LOAD_AUTO,
	HEADER_LIST_PROFILE,
} from '../constants/actionTypes';

const mapStateToProps = state => ({
	...state.profile
});

const mapDispatchToProps = dispatch => ({
	getProfiles: () => 
		dispatch({ type: GET_PROFILES, payload: agent.Profile.getAll()}),
	onChangeInput: username =>
		dispatch({  type: HEADER_CHANGE_INPUT, username}),
	loadAuto: profiles =>
		dispatch({  type: HEADER_LOAD_AUTO, profiles}),
	listProf: username =>
		dispatch({  type: HEADER_LIST_PROFILE, username}),

});

const LoggedOutView = props => {
	if (!props.currentUser) {
		return (
			<ul className="nav navbar-nav pull-xs-right flex-row">

				<li className="nav-item">
					<Link to="/auth/login" className="nav-link">
						Sign in
					</Link>
				</li>

				<li className="nav-item">
					<Link to="/contact" className="nav-link">
						Contact
          			</Link>
				</li>

			</ul>
		);
	}
	return null;
};

const LoggedInView = props => {
	if (props.currentUser) {
		return (
			<ul className="nav navbar-nav pull-xs-right flex-row">

				<li className="nav-item">
					<Link to="/" className="nav-link">
						Home
          			</Link>
				</li>

				<li className="nav-item">
					<Link to="/chats" className="nav-link">
						Chats
					</Link>
				</li>

				<li className="nav-item">
					<Link to="/contact" className="nav-link">
						Contact
          			</Link>
				</li>

				<li className="nav-item">
					<Link
						to={`/@${props.currentUser.username}`}
						className="nav-link">
						<img src={props.currentUser.image} className="user-pic" alt={props.currentUser.username} />
						{props.currentUser.username}
					</Link>
				</li>

			</ul>
		);
	}

	return null;
};



class Header extends React.Component {

	constructor(){
		super();
		this.onChangeInput = ev => {
			this.props.onChangeInput(ev.target.value);

			setTimeout(() => {
				this.props.loadAuto(this.props.profilesAutoG.filter( prof => prof.title.toLowerCase().includes(this.props.valueInput.toLowerCase())))
			},10);

		};

		this.handleResultSelect = (e, { result }) => 
			this.props.listProf(result.title)
	}

	componentWillMount() {
		this.props.getProfiles();
	}

	render() {
		let valueInput = this.props.valueInput;
		return (
			<nav className="navbar navbar-light navHeader">
				<div className="container">
					<Link to="/" className="navbar-brand d-flex flex-row">
						<img src="/slizer.png" alt="logo slizer"/>
						<label className="d-flex align-items-center ml-2 mb-0 colorAppName"> {this.props.appName.toLowerCase()}</label>
					</Link>

					{
						this.props.currentUser ? 
							<Grid>
								<Grid.Column width={6}>
									<Search
										loading={this.props.isLoadingAuto}
										onResultSelect={this.handleResultSelect}
										onSearchChange={this.onChangeInput}
										results={this.props.profilesAuto}
										value={valueInput}
									/>
								</Grid.Column>
							</Grid>
						: ''
					}

					<LoggedOutView currentUser={this.props.currentUser} />

					<LoggedInView currentUser={this.props.currentUser} />
				</div>
			</nav>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
