import PostPreview from './PostPreview';
import ListPagination from './ListPagination';
import React from 'react';
import agent from "../agent";

import { connect } from 'react-redux';
import PostDetails from './PostDetails';

import {
	POST_PAGE_LOADED,
	CLOSE_MODAL,
	ADD_POST_OPINION,
	POST_CHANGE_INPUT,
} from '../constants/actionTypes';

const mapStateToProps = state => ({
	...state.post
});

const mapDispatchToProps = dispatch => ({
	postDetails: payload => 
		dispatch({ type: POST_PAGE_LOADED, payload}),
	onChangeInput: (opinion) =>
		dispatch({  type: POST_CHANGE_INPUT, opinion}),
	addOpinion: (id,opinion) => 
		dispatch({ type: ADD_POST_OPINION, payload: agent.Opinions.create(id,{body: opinion})}),
	onCloseModal: () => 
		dispatch({ type: CLOSE_MODAL}),
});

const PostList = props => {
	if (!props.posts) {
		return (
			<div>Loading...</div>
		);
	}

	if (props.posts.length === 0) {
		return (
			<div>
				No posts are here... yet.
      		</div>
		);
	}

	return (
		<div>
			{
				props.posts.map(post => {
					return (
						<PostPreview post={post} key={post.id} postDetails={props.postDetails}/>
					);
				})
			}

			<ListPagination
			  pager={props.pager}
			  postsCount={props.postsCount}
			  currentPage={props.currentPage} />

			
			<PostDetails
				onChangeInput={props.onChangeInput}
				opinion={props.opinion}
				addOpinion={props.addOpinion}
				postDetails={props.postDetail} 
				postOpinions={props.postOpinions}
                showModal={props.showModal}
                onCloseModal={props.onCloseModal}>
            </PostDetails>
		</div>
	);
};

export default connect(mapStateToProps, mapDispatchToProps)(PostList);
