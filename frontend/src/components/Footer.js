import React from 'react';
import { Link } from 'react-router-dom';
import './App.css';

const Footer = props => {

    return (
        <article className="p-0 m-0 footer">
            <article className="pt-3 pb-3 d-flex justify-content-around align-items-center">
                <section>
                    <img src="/slizer.png" alt="logo slizer"/>
                </section>
                <section className="f-brand">
                    <label className="d-flex align-items-center m-0 colorAppName"> {props.appName.toLowerCase()}</label>
                </section>
                <nav>
                    {props.currentUser ?
                    <ul className="nav n-footer">

                        <li className="nav-item pb-2">
                            <Link to="/" className="f-link">
                                Home
                            </Link>
                        </li>

                        <li className="nav-item pb-2">
                            <Link to="/contact" className="f-link">
                                Contact
                            </Link>
                        </li>

                        <li className="nav-item pb-2">
                            <Link
                                to={`/@${props.currentUser.username}`}
                                className="f-link">
                                Profile
                            </Link>
                        </li>

                    </ul>
                    : ''}
                </nav>
            </article>
            <article className="p-2 f-git">
                <label className="m-0">Proyecto en gitlab: <a href="https://gitlab.com/daniortizgarcia/react_redux_django_rest_framework">Slizer Gitlab</a></label>
            </article>
            <article className="p-2 f-message">
                <label className="m-0">Esta es una aplicación web de prueba realizada en el Grado Superior de 2DAW</label>
            </article>
        </article>
    );

}

export default Footer;
