import React from 'react';
import agent from '../agent';
import './App.css'

const Hashtags = props => {
    if(props.hashtags){
        return props.hashtags.map( hashtag => {
            const handleClick = ev => {
                ev.preventDefault();
                props.onClickHashtag(hashtag.hashtag, page => agent.Posts.byHashtag(hashtag.hashtag, page), agent.Posts.byHashtag(hashtag.hashtag));
            };

            return (
                <label 
                    className="m-1 p-1 rounded hashtag" 
                    key={hashtag.id}
                    onClick={handleClick}>
                    #{hashtag.hashtag}
                </label>
            );
        })
    }else{
        return(
            <label>Loading Hashtags...</label>
        )
    }
}

export default Hashtags;