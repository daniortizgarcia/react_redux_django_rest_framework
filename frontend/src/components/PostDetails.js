import React from 'react';
import Modal from 'react-responsive-modal';
import Author from './Author';
import Opinions from './Opinions';
import './App.css';

const PostDetails = props => {
	const post = props.postDetails;
	const opinion = props.opinion;
	const postOpinions = props.postOpinions;
	const onChangeInput = ev => props.onChangeInput(ev.target.value);
	
	if(post){
		return (
			<div>
				<Modal 
					open={props.showModal} 
					onClose={props.onCloseModal} 
					center
					classNames={{
						overlay: 'customOverlay',
						modal: 'customModal',
					}}>
					<Author author={post.author} />
					<label>{post.post}</label>
					<form className="p-3 mt-3 formHome" onSubmit={(ev) => {props.addOpinion(post.id,opinion); ev.preventDefault();}}>
						<fieldset>
							<input 
								required
								minLength="10"
								maxLength="250"
								className="form-control inputHome" 
								type="text" 
								value={opinion || ''}
								placeholder="Opinion slizer" 
								onChange={onChangeInput}/>
						</fieldset>
					</form>
					{
						postOpinions.map( opinion => {
							return <Opinions opinion={opinion} />
						})
					}
				</Modal>
			</div>
		);
	}else {
		return (<div></div>)
	}
};

export default PostDetails;
