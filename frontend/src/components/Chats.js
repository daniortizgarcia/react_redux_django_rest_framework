import React from 'react';
import { connect } from 'react-redux';
import agent from '../agent';
import ReactDOMServer from 'react-dom/server';

import {
    CHAT_PAGE_LOADED,
    CHAT_SHOW_MESSAGES,
    CHAT_CHANGE_INPUT,
    ADD_MESSAGE,
    CREATE_CHAT,
} from '../constants/actionTypes';

const mapStateToProps = state => ({ 
    chats: state.chat.chats,
    followingUser: state.chat.followingUser,
    showChat: state.chat.showChat,
    message: state.chat.inputChat,
    username: state.common.currentUser.username,
 });

const mapDispatchToProps = dispatch => ({
	onLoad: payload => dispatch({ type: CHAT_PAGE_LOADED, payload }),
	showMessages: (chater, username) => dispatch({ type: CHAT_SHOW_MESSAGES, chater, username }),
	changeInput: message => dispatch({  type: CHAT_CHANGE_INPUT, message}),
    submitForm: payload => dispatch({  type: ADD_MESSAGE, payload}),
    createChat: payload => dispatch({ type: CREATE_CHAT, payload})
})

class Chats extends React.Component {

    constructor(){
        super();
        this.onChangeInput = ev => this.props.changeInput(ev.target.value);
        this.showMessages = (state, username) => this.props.showMessages(state, username);
        this.submitForm = (message) => ev => {
            ev.preventDefault()
            const chat = {creator: this.props.showChat.creator.username, chater: this.props.showChat.chater.username, messages: this.props.showChat.messages+"&/separation/&"+this.props.username+"&/usersepa/&"+message};
            this.props.submitForm(agent.Chat.addMessage(this.props.showChat.id, chat));
        }
        this.createChat = chater => {
            let chat = {messages: this.props.username+'&/usersepa/&'+this.props.username+' a creado el chat'}; 
            this.props.createChat(agent.Chat.createChat(chat, chater))
        }
    }

    componentDidMount() {
        this.props.onLoad(Promise.all([
            agent.Chat.getChats(this.props.username), 
			agent.Profile.getFeed(this.props.username, true),
        ]));
    }

    render(){
        let chargeMessages = () => {
            let messages = this.props.showChat.messages.split("&/separation/&");
            messages = messages.map( message => {
                let dataM = message.split("&/usersepa/&");
                if (dataM[0] === this.props.username)
                    return ReactDOMServer.renderToStaticMarkup(<section className="messageRigth"><label className="messageUser ">{dataM[1]}</label></section>)
                else return ReactDOMServer.renderToStaticMarkup(<section className="messageLeft"><label className="messageUser">{dataM[1]}</label></section>)
            })
            
            return <div dangerouslySetInnerHTML={{__html: messages.join(' ')}}></div>
        }
            let message = this.props.message;
            return(
                <article className="chatsAllContent">
                    <section className="createChat">
                        {this.props.followingUser ? this.props.followingUser.map(user => {
                            return  <section className="userContent" onClick={() => {this.createChat(user.username)}}>
                                        <img width="40px" className="rounded-circle" src={user.image} alt="chater"/>
                                        <label className="chatName chatNameCreate">{user.username}</label>
                                    </section>
                        }): ''}
                    </section>
                    <section className="chatContent pb-0">
                        <h2 className="mb-3 m-0 p-3">Open chat</h2>
                        <h2 className="mb-3 m-0 p-3">Messages</h2>
                    </section>
                    <article className="chatContent">
                        <section className="chaterContent chatColorLeft m-3 p-3">
                        {
                            this.props.chats ?
                            this.props.chats.map( chat => {
                            return  <section>
                                    {
                                        chat.creator.username === this.props.username ? 
                                            <section className="userContent" onClick={() => this.showMessages("chater",chat.chater.username)}>
                                                <img width="50px" className="rounded-circle" src={chat.chater.image} alt="chater"/>
                                                <label className="chatName">{chat.chater.username}</label>
                                            </section>
                                            : <section className="userContent" onClick={() => this.showMessages("creator",chat.creator.username)}>
                                                <img width="50px" className="rounded-circle" src={chat.creator.image} alt="creator"/>
                                                <label className="chatName">{chat.creator.username}</label>
                                            </section>
                                    }
                                    </section>
                            }): ''
                        }
                        </section>
                        <section className="messagesContent chatColorRight m-3 p-5">
                            {
                                this.props.showChat ? <section>{chargeMessages()}</section> : <label>Seleccione algun chat</label>
                            }
                        </section>
                    </article>  
                    <section className="chatContent pt-0">
                        <label className="mb-0 p-3"></label>
                        <form className="p-3 formHome" onSubmit={this.submitForm(message)}>
                            <fieldset>
                                <input 
                                    required
                                    maxLength="150"
                                    className="form-control inputHome" 
                                    type="text" 
                                    placeholder="What's going on?" 
                                    value= {message || ''}
                                    onChange={this.onChangeInput}/>
                            </fieldset>
                        </form>
                    </section>  
                </article>
            );
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Chats)
