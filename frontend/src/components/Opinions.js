import React from 'react';
import Author from './Author';
import './App.css';

const Opinions = props => {
	const opinion = props.opinion;
	
	if(opinion){
		return (
            <div>
                <Author author={opinion.author} />
                <label>{opinion.body}</label>
            </div>
		);
	}else {
		return (<div>No opinions</div>)
	}
};

export default Opinions;
