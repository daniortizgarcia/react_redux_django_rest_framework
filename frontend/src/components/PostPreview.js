import React from 'react';
import Author from './Author';
import agent from '../agent';

const PostPreview = props => {
    const post = props.post;

    return (
        <div className="postContainer">
            <Author author={post.author} />
            <label className="postLabel" onClick={() => props.postDetails(Promise.all([agent.Posts.getOne(post.id),agent.Opinions.forPost(post.id)])) }>{post.post}</label>
        </div>
    );
}
export default PostPreview;