import PostList from './PostList';
import React from 'react';
import Author from './Author';
import CreateInput from './CreateInput';
import agent from '../agent';
import { connect } from 'react-redux';
import {
	FOLLOW_USER,
	UNFOLLOW_USER,
	PROFILE_PAGE_LOADED,
	PROFILE_PAGE_UNLOADED,
	CHANGE_VIEW_PROFILE,
	PROFILE_CHANGE_INPUT,
	HOME_SLIZER_SUBMIT,
	LOGOUT,
} from '../constants/actionTypes';

const mapStateToProps = state => ({
	...state.postList,
	currentUser: state.common.currentUser,
	profile: state.profile
});

const mapDispatchToProps = dispatch => ({
	onFollow: username => dispatch({
		type: FOLLOW_USER, payload: agent.Profile.follow(username)
	}),
	onLoad: payload => dispatch({ type: PROFILE_PAGE_LOADED, payload }),
	onUnfollow: username => dispatch({
		type: UNFOLLOW_USER, payload: agent.Profile.unfollow(username)
	}),
	changeView: (view) => 
		dispatch({ type: CHANGE_VIEW_PROFILE, view }),
	onChangeInput: slizer =>
		dispatch({  type: PROFILE_CHANGE_INPUT, slizer}),
	onSubmit: payload => 
		dispatch({ type: HOME_SLIZER_SUBMIT, payload}),
	onUnload: () => dispatch({ type: PROFILE_PAGE_UNLOADED }),
	onClickLogout: () => dispatch({ type: LOGOUT }),
});

const ProfileLogOut = props => {
	if (props.isUser) {
		return (
			<button
				className="btn btn-outline-danger"
				onClick={() => props.onClickLogout()}>
				Click here to logout.
		  	</button>
		);
	}
	return null;
};

const FollowUserButton = props => {
	if (props.isUser || props.isUser === null) {
		return null;
	}

	let classes = 'btn';
	if (props.user.following) {
		classes += ' btn-unfollow';
	} else {
		classes += ' btn-follow';
	}

	const handleClick = ev => {
		ev.preventDefault();
		if (props.user.following) {
			props.unfollow(props.user.username)
		} else {
			props.follow(props.user.username)
		}
	};

	return (
		<button
			className={classes}
			onClick={handleClick}>
			<i className="ion-plus-round"></i>
			&nbsp;
      		{props.user.following ? 'Unfollow' : 'Follow'} {props.user.username}
		</button>
	);
};

class Profile extends React.Component {

	constructor() {
		super();
		this.onChangeInput = input => this.props.onChangeInput(input);
		this.submitForm = post => {
			this.props.onSubmit(agent.Posts.create(post));
		}
	}

	componentWillMount() {
		this.props.onLoad(Promise.all([
			agent.Profile.get(this.props.match.params.username),
			agent.Posts.byAuthor(this.props.match.params.username),
			agent.Profile.getFeed(this.props.match.params.username, false),
			agent.Profile.getFeed(this.props.match.params.username, true),
		]));
	}

	componentWillUnmount() {
		this.props.onUnload();
	}

	renderTabs() {
		return (
			<ul className="nav tabs">
				<li onClick={() => this.props.changeView('posts')}>
					{this.props.profile.username ? this.props.profile.username.toUpperCase() : ''} Posts
				</li>

				<li onClick={() => this.props.changeView('following')}>
					{this.props.profile.followingUser ? this.props.profile.followingUser.length : ''} following
				</li>

				<li onClick={() => this.props.changeView('followers')}>
					{this.props.profile.followersUser ? this.props.profile.followersUser.length : ''} followers
				</li>
			</ul>
		);

	}

	render() {
		let slizer = this.props.profile.slizer;
		const profile = this.props.profile;
		if (!profile) {
			return null;
		}
		if (this.props.profile.username && this.props.profile.username !== this.props.match.params.username)
			window.location.reload();

		const isUser = this.props.currentUser &&
			this.props.profile.username === this.props.currentUser.username;
		return (
			<div>
				<div className="profileHeader">
					<img width="150px" src={profile.image} className="user-img" alt={profile.username} />
					<div>
						<h1>{profile.username}</h1>
						<p>{profile.bio}</p>
					</div>
				</div>

				<nav className="profileNav">
					{this.renderTabs()}
					<div>
						<ProfileLogOut isUser={isUser} onClickLogout={this.props.onClickLogout} />
						<FollowUserButton
							isUser={isUser}
							user={profile}
							follow={this.props.onFollow}
							unfollow={this.props.onUnfollow} />
					</div>
				</nav>

				<div className="footerContent">
					<div className="col-xs-12 col-md-10 offset-md-1">
						<CreateInput 
							slizer={slizer}
							submitForm={this.submitForm}
							changeInput={this.onChangeInput}/>
						<div>
							<label className="viewTitle">{this.props.profile.view}</label>
							{
								this.props.profile.view === 'posts' ? 
								<PostList
									pager={this.props.pager}
									posts={this.props.posts}
									postsCount={this.props.postsCount}
									state={this.props.currentPage} /> : ''
							}

							{
								this.props.profile.followingUser && this.props.profile.view === 'following' ? this.props.profile.followingUser.map((profile) => {
									return <Author author={profile} />
								}) : ''
							}

							{
								this.props.profile.followersUser && this.props.profile.view === 'followers' ? this.props.profile.followersUser.map((profile) => {
									return <Author author={profile} />
								})  : ''
							}
						</div>
					</div>
				</div>

			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
