
import React from 'react';

const CreateInput = props => {
    let slizer = props.slizer;

    this.onChangeInput = ev => props.changeInput(ev.target.value);
    this.submitForm = (slizer) => ev => {
        let hashtagList = slizer.match(/(#[a-zA-Z0-9]+)/g);
        if(hashtagList)
            hashtagList = hashtagList.map(hashtag =>{
                return hashtag.slice(1)
            });
        else 
            hashtagList = []
        const post = {post:slizer,hashtagList:hashtagList};
        props.submitForm(post);
    }

	return (
        <form className="p-3 mb-3 formHome" onSubmit={this.submitForm(slizer)}>
            <fieldset>
                <input 
                    required
                    minLength="10"
                    maxLength="250"
                    className="form-control inputHome" 
                    type="text" 
                    placeholder="What's going on?" 
                    value= {slizer || ''}
                    onChange={this.onChangeInput}/>
            </fieldset>
        </form>
	);
};

export default CreateInput;
