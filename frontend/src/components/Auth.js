import { Link } from 'react-router-dom';
import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import {
    UPDATE_FIELD_AUTH,
    REGISTER,
    LOGIN,
    AUTH_PAGE_UNLOADED,
} from '../constants/actionTypes';

const mapStateToProps = state => ({ ...state.auth });

const mapDispatchToProps = dispatch => ({
    onChangeField: (value,key) =>
        dispatch({ type: UPDATE_FIELD_AUTH, key: key, value }),
    register: (username, email, password) =>
        dispatch({ type: REGISTER, payload: agent.Auth.register(username, email, password) }),
    login: (username, email, password) =>
        dispatch({ type: LOGIN, payload: agent.Auth.login(email, password) }),
    onUnload: () =>
        dispatch({ type: AUTH_PAGE_UNLOADED })
});

class Auth extends React.Component {

    constructor(){
        super();
        this.changeUsername = ev => this.props.onChangeField(ev.target.value,"username");
        this.changeEmail = ev => this.props.onChangeField(ev.target.value,"email");
        this.changePassword = ev => this.props.onChangeField(ev.target.value,"password");
        this.submitForm = (username = '', email, password) => ev => {
            ev.preventDefault();
            console.log(username, email, password);
            this.props[this.props.match.params.type](username, email, password);
        }
    }



  componentWillUnmount() {
    this.props.onUnload();
  }

    render() {
        const username = this.props.username;
        const email = this.props.email;
        const password = this.props.password;

        return (
        <div className="authContent">
            <div className="photoContent">
                
            </div>
            <div className="authformContent">

                <img src="/slizer.png" alt="logo slizer"/>
                <h1>Descubre lo que está<br />pasando en el mundo en<br /> este momento</h1>
                
                <h1 className="text-xs-center">
                    {
                        this.props.match.params.type === 'register' ?
                            <label className="mb-0">Sign Up</label>
                        :   <label className="mb-0">Sign In</label>
                    }
                </h1>
                <p className="text-xs-center">
                    {
                        this.props.match.params.type === 'register' ?
                            <Link to="/auth/login">
                                Have an account?
                            </Link>
                        :   <Link to="/auth/register">
                                Need an account?
                            </Link>
                    }
                </p>

                <ListErrors errors={this.props.errors} />

                <form width="50%" onSubmit={this.submitForm(username, email, password)}>
                    <fieldset>
                        {
                            this.props.match.params.type === 'register' ?
                                <fieldset className="form-group">
                                    <input
                                        className="form-control form-control-lg"
                                        type="text"
                                        value={username || ''}
                                        placeholder="Username"
                                        onChange={this.changeUsername}/>
                                </fieldset>
                            : ''
                        }
                    <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="email"
                            value={email || ''}
                            placeholder="Email"
                            onChange={this.changeEmail}/>
                    </fieldset>

                    <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="password"
                            value={password || ''}
                            placeholder="Password"
                            onChange={this.changePassword}/>
                    </fieldset>
                        
                    <button
                        className="btn btn-lg btn-primary pull-xs-right"
                        type="submit">
                        {
                            this.props.match.params.type === 'register' ?
                                'SignUp'
                            :   'SignIn'
                        }
                    </button>

                    </fieldset>
                </form>
            </div>
        </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
