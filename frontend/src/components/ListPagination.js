import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { SET_PAGE } from '../constants/actionTypes';


const mapStateToProps = state => ({
	username: state.common.currentUser ? state.common.currentUser.username : '',
});

const mapDispatchToProps = dispatch => ({
  onSetPage: (page, payload, username) =>
    dispatch({ type: SET_PAGE, page, payload, username })
});

const ListPagination = props => {
  if (props.postsCount <= 10) {
    return null;
  }
  const range = [];
  for (let i = 0; i < Math.ceil(props.postsCount / 10); ++i) {
    range.push(i);
  }

  const setPage = page => {
    if(props.pager) {
      props.onSetPage(page, props.pager(page), props.username);
    }else {
      props.onSetPage(page, agent.Posts.getAll(page), props.username)
    }
  };

  return (
    <nav>
      <ul className="pagination">

        {
          range.map(v => {
            const isCurrent = v === props.currentPage;
            const onClick = ev => {
              ev.preventDefault();
              setPage(v);
            };
            return (
              <li
                className={ isCurrent ? 'page-item active' : 'page-item' }
                onClick={onClick}
                key={v.toString()}>

                <a className="page-link" href="">{v + 1}</a>

              </li>
            );
          })
        }

      </ul>
    </nav>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ListPagination);
