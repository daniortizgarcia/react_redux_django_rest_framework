
import React from 'react';
import { connect } from 'react-redux';
import PostList from '../PostList';

const mapStateToProps = state => ({
	...state.postList
});

const MainView = props => {
	return (
		<div>
			<PostList
				posts={props.posts} 
				pager={props.pager}
				postsCount={props.postsCount} 
				currentPage={props.currentPage} />
		</div>
	);
};

export default connect(mapStateToProps)(MainView);
