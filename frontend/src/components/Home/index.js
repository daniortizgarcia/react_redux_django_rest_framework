import MainView from './MainView';
import React from 'react';
import '../App.css';
import Hashtags from '../Hashtags';
import CreateInput from '../CreateInput';
import agent from '../../agent';
import { connect } from 'react-redux';
import { FaGlobeAmericas, FaUserCircle } from 'react-icons/fa';
import {
	HOME_PAGE_LOADED,
	HOME_PAGE_UNLOADED,
	APPLY_HASHTAG_FILTER,
	HOME_CHANGE_INPUT,
	HOME_SLIZER_SUBMIT,
	APP_LOAD,
	HOME_CHANGE_FEED,
	UNAUTHORIZED
} from '../../constants/actionTypes';

const mapStateToProps = state => ({
	...state.home,
	appName: state.common.appName,
	username: state.common.currentUser ? state.common.currentUser.username : '',
	token: state.common.token
});

const mapDispatchToProps = dispatch => ({
	onLoad: (pager, payload) =>
		dispatch({ type: HOME_PAGE_LOADED, pager, payload }),
	onLoadUser: (payload, token) =>
		dispatch({ type: APP_LOAD, payload, token, skipTracking: true }),
	onClickHashtag: (hashtag, pager, payload) =>
		dispatch({ type: APPLY_HASHTAG_FILTER, hashtag, pager, payload }),
	onChangeInput: slizer =>
		dispatch({  type: HOME_CHANGE_INPUT, slizer}),
	onSubmit: payload => 
		dispatch({ type: HOME_SLIZER_SUBMIT, payload}),
	changeFeed: (pager, payload, username) =>
		dispatch({ type: HOME_CHANGE_FEED, pager, payload, username}),
	onUnload: () =>
		dispatch({  type: HOME_PAGE_UNLOADED }),
	unauthorized: () => 
		dispatch({  type: UNAUTHORIZED }),
});

class Home extends React.Component {

	constructor(){
		super();
		this.onChangeInput = input => this.props.onChangeInput(input);
		this.submitForm = post => {
			this.props.onSubmit(agent.Posts.create(post));
		}
	}

	componentWillMount() {
		if(this.props.token) {
			this.props.onLoadUser(agent.Auth.current(), this.props.token)
			this.props.onLoad(agent.Posts.getFeed, Promise.all([agent.Posts.getFeed(),agent.Hashtags.getAll()]))
		}else
			this.props.unauthorized();
	}

	componentWillUnmount() {
		this.props.onUnload();
	}

	render() {
		let slizer = this.props.slizer;
		return (
			<div className="pt-4 pb-5 pl-3 pr-3 contentHome">
				<div className="totallyInfo center-block">
					<div className="contentIcons">
						<div onClick={() => this.props.changeFeed(agent.Posts.getFeed, agent.Posts.getFeed(), this.props.username)}>
							<FaUserCircle />
						</div>
						<div onClick={() => this.props.changeFeed(agent.Posts.getAll, agent.Posts.getAll(), this.props.username)}>
							<FaGlobeAmericas />
						</div>
					</div>
					<CreateInput 
						slizer={slizer}
						submitForm={this.submitForm}
						changeInput={this.onChangeInput}/>
					<section>
						<MainView />
					</section>
				</div>
				<div className="noticeHome">
					<div className="d-flex flex-column">
						<h5>Suggested people</h5>
						<div className="d-flex mt-2 mb-1">
							<div>
								<img width="65px" className="rounded-circle" src="https://lh3.googleusercontent.com/-8VxDmCmO70M/AAAAAAAAAAI/AAAAAAAAAAA/ACevoQMOhhgj6jWOpT_Xe0kIar_QXopGXg/s96-c-mo/photo.jpg" alt="img prueba" />
							</div>
							<div className="align-text-bottom">
								<label className="m-0 ml-1">Dani Ortiz</label><br />
								<label className="m-0 ml-1">@daniortiz</label>
							</div>
						</div>
						<div className="d-flex mt-2 mb-1">
							<div>
								<img width="65px" className="rounded-circle" src="https://lh3.googleusercontent.com/-8VxDmCmO70M/AAAAAAAAAAI/AAAAAAAAAAA/ACevoQMOhhgj6jWOpT_Xe0kIar_QXopGXg/s96-c-mo/photo.jpg" alt="img prueba" />
							</div>
							<div className="align-text-bottom">
								<label className="m-0 ml-1">Dani Ortiz</label><br />
								<label className="m-0 ml-1">@daniortiz</label>
							</div>
						</div>
					</div>
					<div className="mt-3 mb-2">
						<h5>Suggested Hashtags</h5>

						<label 
							className="m-1 p-1 rounded hashtag"
							onClick={() => {this.props.onLoad(agent.Posts.getAll, Promise.all([agent.Posts.getFeed(), agent.Hashtags.getAll()]))}}>
							#All </label>
						<Hashtags 
							hashtags={this.props.hashtags}
							onClickHashtag={this.props.onClickHashtag}/>
					</div>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);