import React from 'react';
import { Link } from 'react-router-dom';

const Author = props => {
    const author = props.author;

    return (
        <Link to={`/@${author.username}`}>
            <div className="d-flex mt-2 mb-1">
                <div>
                    <img width="48px" className="rounded-circle" src={author.image} alt="img prueba" />
                </div>
                <div className="d-flex flex-column justify-content-end ml-2">
                    <label className="m-0"><strong>{author.username}</strong></label>
                    <label className="m-0 text-muted">@{author.username}</label>
                </div>
            </div>
        </Link>
    );
}

export default Author;
