PROYECTO REALIZADO A PARTIR DE THINKSTER

#HOME (Logged In)
- Listado de los posts que sigues
- Listado de todos los posts menos los tuyos.
- Listado de todos los tags.
- Listado filtrado por los tags.
- Paginación de los posts.
- Crear un nuevo post.

#HEADER
- Una busqueda autocompletada de los usuarios, mostrando su usuario, imagen, descripción utilizando semantic ui.
- Mostrar unos links o otros.

#CONTACT
- Enviar Correo con validación en front-end i back-end.

#AUTH (Logged out)
- Utiliza el mismo componente para el login y el register.
- Al crearlo te asigna una imagen aleatoria.

#LIST POSTS (Logged In)
- Se abre un modal con el post y sus comentarios.
- Puedes añadir comentarios al post.

#PROFILE (Logged In)
- Perfil con la imagen, username y descripción.
- Puedes ver sus posts, seguidos y seguidores.
- Puedes crear un post.

#CHATS (Logged In)
- Puedes crear chats.
- Puedes hablar con la persona que sigues.

#OTRAS MEJORAS
- Css personalizado en toda la aplicación.
- Icono con formato svg realizado con Inkscape.