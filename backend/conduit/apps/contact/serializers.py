from rest_framework import serializers

class ContactSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=13)
    email = serializers.CharField(max_length=255)
    subject = serializers.CharField(max_length=255)
    message = serializers.CharField(max_length=75)

    def validate(self, data):
        name = data.get('name', None)
        email = data.get('email', None)
        subject = data.get('subject', None)
        message = data.get('message', None)

        if name is None:
            raise serializers.ValidationError(
                'An name is required to send email.'
            )

        if email is None:
            raise serializers.ValidationError(
                'An email address is required to send email.'
            )

        if subject is None:
            raise serializers.ValidationError(
                'An Subject address is required to send email.'
            )
            
        if message is None:
            raise serializers.ValidationError(
                'A message is required to send email.'
            )
            
        return {
            'name':  name,
            'email': email,
            'subject': subject,
            'message': message
        }