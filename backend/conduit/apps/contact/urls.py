from django.conf.urls import url

from rest_framework.routers import DefaultRouter

from .views import (
    SendEmailView
)

urlpatterns = [
    url(r'^contact/?$', SendEmailView.as_view()),
]