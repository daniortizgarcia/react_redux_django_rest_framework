from django.apps import AppConfig


class ContactAppConfig(AppConfig):
    name = 'conduit.apps.contact'
    label = 'contact'
    verbose_name = 'Contact'

default_app_config = 'conduit.apps.contact.ContactAppConfig'
