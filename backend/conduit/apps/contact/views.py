from django.core.mail import send_mail, BadHeaderError
from rest_framework import generics, mixins, status, viewsets
from rest_framework.exceptions import NotFound

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny

from .serializers import (
    ContactSerializer
)

class SendEmailView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = ContactSerializer
    
    def post(self, request):
        email = request.data.get('email', {})

        serializer = self.serializer_class(data=email)
        serializer.is_valid(raise_exception=True)

        try:
            send_mail(email.get('subject', None), 'Message from '+email.get('name', None) +': \n' + email.get('message', None), 'slizer@gmail.com', [email.get('email', None)])
        except BadHeaderError:
            return Response({
                    'status': 'false',
                    'message': 'BadHeaderError for your message'
                }, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        return Response({
            'status': 'true',
            'message': 'The message sent correcly'
        }, status=status.HTTP_200_OK)