from rest_framework import serializers
from .models import Chat
from conduit.apps.profiles.serializers import ProfileSerializer

class ChatSerializer(serializers.ModelSerializer):
    creator = ProfileSerializer(read_only=True)
    chater = ProfileSerializer(read_only=True)
    messages = serializers.CharField()

    class Meta:
        model = Chat
        fields = (
            'id',
            'messages',
            'creator',
            'chater',
        )
    
    def create(self, validated_data):
        creator = self.context.get('creator', None)
        chater = self.context.get('chater', None)

        chat = Chat.objects.create(creator=creator, chater=chater, **validated_data)

        return chat