from django.conf.urls import url

from rest_framework.routers import DefaultRouter

from .views import (
    ChatsListCreateAPIView,
    ChatRetrieveUpdateAPIView,
)

urlpatterns = [
    url(r'^chats/?$', ChatsListCreateAPIView.as_view()),
    url(r'^chat/(?P<id>\d+)/?$', ChatRetrieveUpdateAPIView.as_view()),
]