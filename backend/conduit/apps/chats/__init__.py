from django.apps import AppConfig


class ChatsAppConfig(AppConfig):
    name = 'conduit.apps.chats'
    label = 'chats'
    verbose_name = 'Chats'

default_app_config = 'conduit.apps.chats.ChatsAppConfig'
