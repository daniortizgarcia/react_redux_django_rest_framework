from django.db import models

from conduit.apps.core.models import TimestampedModel

class Chat(TimestampedModel):
    messages = models.TextField()

    creator = models.ForeignKey(
        'profiles.Profile', on_delete=models.CASCADE, related_name='creator'
    )

    chater = models.ForeignKey(
        'profiles.Profile', on_delete=models.CASCADE, related_name='chater'
    )
    
    def __str__(self):
        return self.messages