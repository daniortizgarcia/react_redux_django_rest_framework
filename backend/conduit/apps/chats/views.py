from rest_framework import generics, mixins, status, viewsets
from rest_framework.exceptions import NotFound

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import (AllowAny, IsAuthenticatedOrReadOnly, IsAuthenticated)

from models import *
from conduit.apps.profiles.models import Profile

from .serializers import (
    ChatSerializer,
)

class ChatsListCreateAPIView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Chat.objects.all()
    serializer_class = ChatSerializer

    def get(self, request):
        serializer_context = {'request': request}
        username =  self.request.query_params.get('username', None)

        try:
            chatcr = self.queryset.filter(creator__user__username=username)
            chatch = self.queryset.filter(chater__user__username=username)
            chat = chatcr | chatch
        except Chat.DoesNotExist:
            raise NotFound('Chat Not Found.')

        if chat.exists():
            serializer = self.serializer_class(
                chat,
                context=serializer_context,
                many=True
            )

            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(False, status=status.HTTP_200_OK)

    def create(self, request):
        serializer_data = request.data.get('chat', {})
        chater = Profile.objects.get(user__username=request.data.get('chater', {}))

        serializer_context = { 
            'creator': request.user.profile, 
            'chater': chater, 
            'request': request 
        }
        
        serializer = self.serializer_class(
            data=serializer_data, context=serializer_context
        )
        
        serializer.is_valid(raise_exception=True)

        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

class ChatRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = Chat.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = ChatSerializer

    def update(self, request, id):
        serializer_context = {'request': request}

        try:
            serializer_instance = self.queryset.get(id=id)
        except Article.DoesNotExist:
            raise NotFound('Chat not exists.')
            
        serializer_data = request.data.get('chat', {})

        serializer = self.serializer_class(
            serializer_instance, 
            context=serializer_context,
            data=serializer_data, 
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)
            