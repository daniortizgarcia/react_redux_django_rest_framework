from django.conf.urls import url

from .views import ProfileListAPIView, ProfileRetrieveAPIView, ProfileFollowAPIView, ProfilesFeedListAPIView

urlpatterns = [
    url(r'^profiles/?$', ProfileListAPIView.as_view()),
    url(r'^profiles/(?P<username>\w+)/feed/(?P<condition>\w+)/?$', ProfilesFeedListAPIView.as_view()),
    url(r'^profiles/(?P<username>\w+)/?$', ProfileRetrieveAPIView.as_view()),
    url(r'^profiles/(?P<username>\w+)/follow/?$', 
        ProfileFollowAPIView.as_view()),
]
