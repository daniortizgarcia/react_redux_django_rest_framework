from rest_framework import generics, serializers, status
from rest_framework.exceptions import NotFound
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Profile
from .renderers import ProfileJSONRenderer
from .serializers import ProfileSerializer

class ProfileListAPIView(generics.ListAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    '''def get_queryset(self):
        queryset = self.queryset

        username = self.request.query_params.get('username', None)
        if username is not None:
            queryset = self.queryset.filter(user__username__icontains=username)

        return queryset

    def list(self, request):
        serializer_context = {'request': request}
        queryset = self.get_queryset()

        serializer = self.serializer_class(
            queryset,
            context=serializer_context,
            many=True
        )

        return Response(serializer.data, status=status.HTTP_200_OK)'''

class ProfilesFeedListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Profile.objects.select_related('user')
    serializer_class = ProfileSerializer

    def list(self, request, username, condition):

        try:
            prof = self.queryset.get(user__username=username)
            if condition == 'false':
                profile = Profile.objects.filter(follows__in=[prof])
            else:
                profile = Profile.objects.all()

        except Profile.DoesNotExist:
            raise NotFound('A profile with this username does not exist.')

        serializer_context = {'request': request,'usernameFeed': prof}

        serializer = self.serializer_class(
            profile,context=serializer_context, many=True,
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

class ProfileRetrieveAPIView(RetrieveAPIView):
    permission_classes = (AllowAny,)
    queryset = Profile.objects.select_related('user')
    renderer_classes = (ProfileJSONRenderer,)
    serializer_class = ProfileSerializer

    def retrieve(self, request, username, *args, **kwargs):
        # Try to retrieve the requested profile and throw an exception if the
        # profile could not be found.
        try:
            profile = self.queryset.get(user__username=username)
        except Profile.DoesNotExist:
            raise NotFound('A profile with this username does not exist.')

        serializer = self.serializer_class(profile, context={
            'request': request
        })

        return Response(serializer.data, status=status.HTTP_200_OK)


class ProfileFollowAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (ProfileJSONRenderer,)
    serializer_class = ProfileSerializer

    def delete(self, request, username=None):
        follower = self.request.user.profile

        try:
            followee = Profile.objects.get(user__username=username)
        except Profile.DoesNotExist:
            raise NotFound('A profile with this username was not found.')

        follower.unfollow(followee)

        serializer = self.serializer_class(followee, context={
            'request': request
        })

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, username=None):
        follower = self.request.user.profile

        try:
            followee = Profile.objects.get(user__username=username)
        except Profile.DoesNotExist:
            raise NotFound('A profile with this username was not found.')

        if follower.pk is followee.pk:
            raise serializers.ValidationError('You can not follow yourself.')

        follower.follow(followee)

        serializer = self.serializer_class(followee, context={
            'request': request
        })

        return Response(serializer.data, status=status.HTTP_201_CREATED)
