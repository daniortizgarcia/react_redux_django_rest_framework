from django.apps import AppConfig


class PostsAppConfig(AppConfig):
    name = 'conduit.apps.posts'
    label = 'posts'
    verbose_name = 'Posts'

default_app_config = 'conduit.apps.posts.PostsAppConfig'
